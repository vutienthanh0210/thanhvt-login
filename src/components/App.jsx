import React, { useState } from "react";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import Box from "@mui/material/Box";
import AccountBalanceIcon from "@mui/icons-material/AccountBalance";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { gql, useMutation } from "@apollo/client"; 

const LOGIN = gql`
	mutation Login($accountNumber: String, $password: String) {
		login(accountNumber: $accountNumber, password: $password) {
			success
      user {
        accountNumber
        balance
        displayName
        id
      }
		}
	}
`;

const defaultTheme = createTheme();

export default function SignIn() {
	const [submitLogin, { data, loading, error }] = useMutation(LOGIN);

	const handleSubmit = async (event) => {
		event.preventDefault();
		const formData = new FormData(event.currentTarget);
		await submitLogin({ variables: {
			accountNumber: formData.get("accountNumber"),
			password: formData.get("password"),
		}});
		if (data) {
			const {
				login: { success, user },
			} = data;
			if (success) {
				localStorage.setItem("user", JSON.stringify(user));
				window.location = "/dashboard";
			}
		} else {
			alert("Login failed");
		}
	};

	return (
		<ThemeProvider theme={defaultTheme}>
			<Container component="main" maxWidth="xs">
				<CssBaseline />
				<Box
					sx={{
						marginTop: 8,
						display: "flex",
						flexDirection: "column",
						alignItems: "center",
					}}
				>
					<Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
						<AccountBalanceIcon />
					</Avatar>
					<Typography component="h1" variant="h5">
						MicroBank
					</Typography>
					<Box
						component="form"
						onSubmit={handleSubmit}
						noValidate
						sx={{ mt: 1 }}
					>
						<TextField
							margin="normal"
							required
							fullWidth
							id="accountNumber"
							label="Account number"
							name="accountNumber"
							autoComplete="accountNumber"
							autoFocus
						/>
						<TextField
							margin="normal"
							required
							fullWidth
							name="password"
							label="Password"
							type="password"
							id="password"
							autoComplete="current-password"
						/>
						<Button
							type="submit"
							fullWidth
							variant="contained"
							sx={{ mt: 3, mb: 2 }}
						>
							Login
						</Button>
					</Box>
				</Box>
			</Container>
		</ThemeProvider>
	);
}
